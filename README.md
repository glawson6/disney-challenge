
# API Challenge

Design and create a RESTful API that could be used to manage a list of dog images. The API is meant to act as the backend to the [Dog Breed App](https://gist.github.com/maxdisney/26e421c68f5d543be5b2), which is being built by a different team.

# Requirements

The operations we expect to see would be:

* List all of the available dog pictures grouped by breed
* List all of the available dog pictures of a particular breed
* Vote up and down a dog picture
* The details associated with a dog picture

The information the Dog Breed App needs to function is:

* A URL to a dog picture
* The number of time the picture was favorited
* The dog's breed
* Any other information required to identify a specific dog

The Dog Breed App expects the response to be sorted by the number of times the picture was favorited. The pictures may be sorted

The API responses must be in JSON.

## Additional Voting Requirements

Each client is allowed to vote once for any particular dog picture.

# Setup

We provide you with a starter Spring Boot project. The project is already configured go use Spring MVC and talks to an in memory HSQLDB to store the results. Jackson is already included to provide JSON serialization and deserialization.

The Spring Boot Started Test module is included to facilitate both unit and integration tests.

Feel free to pick a different framework if you feel more comfortable doing so.

## Dependencies

This project uses Maven for builds.
You need Java 8 installed.

You need a way to expose and demo the end point you are building. [ngrok](https://ngrok.com/) is a fine way to achieve this goal.

## Checking Out the Project

The project is hosted here on Gitlab.com.

## Building

```
$ mvn package
```

## Running

You can run the app through Maven:

```
$ mvn spring-boot:run
```

or you can run the jar file from the build:

```
$ java -jar target/api_interview-0.1.0.jar
```

# Build, Deployment and Running

While this falls outside of the challenge, please consider the following as you get ready to discuss your solution with the team:

* How would you package this for deployment?
* How and where would you deploy this app?
* How can you tell that the app is up and running?
* How would you configure the app as it goes from dev, to qa and finally to production?
* How would you insulate the app from a downstream API if it had one.

# Submitting the Project

You will also receive an invitation to your own private repository on gitlab.com. Push your code to that repository as
you go. Instructions are provided by gitlab.com on how to push your code to a new repo.

# Duration

You will have until the end of the day to complete the challenge. In our tests, we found it to take us about 3 hours to build. We encourage you to spend any leftover time to polish/document/test your app. Please send us a zip file of your challenge when completed.

The timestamp of your last commit will be used to gage how long you worked on this project.


# API
* List all of the available dog pictures grouped by breed
GET /api/dogs HTTP/1.1

* List all of the available dog pictures of a particular breed
ET /api/dogs?breedName=Yorkie HTTP/1.1

* Vote up and down a dog picture
POST /api/votes HTTP/1.1
Accept: application/json, */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Content-Length: 112
Content-Type: application/json
Host: localhost:8080
User-Agent: HTTPie/0.9.9

{
    "dogId": "d156a3f4-aa5d-4510-bcc8-f29820968d12", 
    "judgeId": "bc5946ff-a8bb-4f0b-b21a-93a795c9d635", 
    "score": 1
}


* The details associated with a dog picture 
GET /api/dogs/d156a3f4-aa5d-4510-bcc8-f29820968d12 HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: localhost:8080
User-Agent: HTTPie/0.9.9



HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Fri, 12 Jan 2018 03:42:18 GMT
Transfer-Encoding: chunked

{
    "breed": {
        "breedName": "Yorkie", 
        "id": "4c9803f8-44db-45b3-9abc-12cf39ff9795"
    }, 
    "dogName": "Yorkie-3", 
    "dogURL": "http://i.imgur.com/qWLKy8a.jpg", 
    "id": "d156a3f4-aa5d-4510-bcc8-f29820968d12"
}


* votes
GET /api/votes HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: localhost:8080
User-Agent: HTTPie/0.9.9



HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Fri, 12 Jan 2018 03:42:53 GMT
Transfer-Encoding: chunked

{
    "Labrador": [
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-12", 
            "dogURL": "http://i.imgur.com/BYvRbs8.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-2", 
            "dogURL": "http://i.imgur.com/xX2AeDR.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-3", 
            "dogURL": "http://i.imgur.com/hBFRUuW.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-11", 
            "dogURL": "http://i.imgur.com/SAJJ1oH.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-10", 
            "dogURL": "http://i.imgur.com/kSU7Zca.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-4", 
            "dogURL": "http://i.imgur.com/WDWK4nF.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-5", 
            "dogURL": "http://i.imgur.com/zxtD5Zw.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-6", 
            "dogURL": "http://i.imgur.com/MrkAGKR.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-7", 
            "dogURL": "http://i.imgur.com/o3Nyw4R.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-15", 
            "dogURL": "http://i.imgur.com/gskym.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-8", 
            "dogURL": "http://i.imgur.com/SzP5370.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-14", 
            "dogURL": "http://i.imgur.com/qigJZWa.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-9", 
            "dogURL": "http://i.imgur.com/oHaP6I3.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-13", 
            "dogURL": "http://i.imgur.com/VzFTsGg.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Labrador", 
            "dogName": "Labrador-1", 
            "dogURL": "http://i.imgur.com/eE29vX4.png", 
            "voteCount": 0
        }
    ], 
    "Pug": [
        {
            "breedName": "Pug", 
            "dogName": "Pug-11", 
            "dogURL": "http://i.imgur.com/Fl2ivvG.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-33", 
            "dogURL": "http://i.imgur.com/ZwM4KY4.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-10", 
            "dogURL": "http://i.imgur.com/IOh5mgB.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-32", 
            "dogURL": "http://i.imgur.com/NSCW1Rz.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-31", 
            "dogURL": "http://i.imgur.com/9YfjSxU.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-30", 
            "dogURL": "http://i.imgur.com/8B3HOrS.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-37", 
            "dogURL": "http://i.imgur.com/mNQbxWo.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-15", 
            "dogURL": "http://i.imgur.com/7ysJzS4.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-36", 
            "dogURL": "http://i.imgur.com/6Fz5JOg.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-14", 
            "dogURL": "http://i.imgur.com/8c3RpI0.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-35", 
            "dogURL": "http://i.imgur.com/ZFk0cgV.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-13", 
            "dogURL": "http://i.imgur.com/RQKBN3F.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-34", 
            "dogURL": "http://i.imgur.com/uy9pY3Y.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-12", 
            "dogURL": "http://i.imgur.com/iLzWvSY.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-9", 
            "dogURL": "http://i.imgur.com/Mda3xXr.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-19", 
            "dogURL": "http://i.imgur.com/xAGJ0Ry.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-18", 
            "dogURL": "http://i.imgur.com/tUZhJYN.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-17", 
            "dogURL": "http://i.imgur.com/HrscSnK.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-7", 
            "dogURL": "http://i.imgur.com/Flic3TB.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-39", 
            "dogURL": "http://i.imgur.com/Rdbawjx.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-8", 
            "dogURL": "http://i.imgur.com/zjgtrf9.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-38", 
            "dogURL": "http://i.imgur.com/C1MFoB0.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-16", 
            "dogURL": "http://i.imgur.com/uccGfLn.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-5", 
            "dogURL": "http://i.imgur.com/NGG3Yir.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-6", 
            "dogURL": "http://i.imgur.com/q53cfRy.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-3", 
            "dogURL": "http://i.imgur.com/miiP4NI.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-4", 
            "dogURL": "http://i.imgur.com/GCE8dj5.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-1", 
            "dogURL": "http://i.imgur.com/ozJD7SC.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-2", 
            "dogURL": "http://i.imgur.com/E5vBM5Z.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-22", 
            "dogURL": "http://i.imgur.com/umJXx6S.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-21", 
            "dogURL": "http://i.imgur.com/Y32LWO6.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-20", 
            "dogURL": "http://i.imgur.com/YxSvzWm.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-26", 
            "dogURL": "http://i.imgur.com/RUjOi8t.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-25", 
            "dogURL": "http://i.imgur.com/zUnR5lj.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-24", 
            "dogURL": "http://i.imgur.com/xTg9j70.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-23", 
            "dogURL": "http://i.imgur.com/R8Ju76x.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-29", 
            "dogURL": "http://i.imgur.com/7YxiavD.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-28", 
            "dogURL": "http://i.imgur.com/Q5uksG8.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Pug", 
            "dogName": "Pug-27", 
            "dogURL": "http://i.imgur.com/Dd1K1uR.png", 
            "voteCount": 0
        }
    ], 
    "Retriever": [
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-2", 
            "dogURL": "http://i.imgur.com/CHRlo0W.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-1", 
            "dogURL": "http://i.imgur.com/wR38uBx.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-32", 
            "dogURL": "http://i.imgur.com/BnhbY54.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-4", 
            "dogURL": "http://i.imgur.com/VCgfaB2.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-10", 
            "dogURL": "http://i.imgur.com/gpH8wIV.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-11", 
            "dogURL": "http://i.imgur.com/luPxoig.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-33", 
            "dogURL": "http://i.imgur.com/ToYsMEC.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-3", 
            "dogURL": "http://i.imgur.com/tXnch1O.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-6", 
            "dogURL": "http://i.imgur.com/Ful5PwH.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-30", 
            "dogURL": "http://i.imgur.com/BYvRbs8.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-31", 
            "dogURL": "http://i.imgur.com/VzFTsGg.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-5", 
            "dogURL": "http://i.imgur.com/Ror46i4.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-14", 
            "dogURL": "http://i.imgur.com/ZSzHQ2q.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-15", 
            "dogURL": "http://i.imgur.com/PoA6rLg.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-12", 
            "dogURL": "http://i.imgur.com/MiIDyfD.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-13", 
            "dogURL": "http://i.imgur.com/4qmmRFj.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-18", 
            "dogURL": "http://i.imgur.com/NinMpda.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-19", 
            "dogURL": "http://i.imgur.com/4L8rMko.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-16", 
            "dogURL": "http://i.imgur.com/NYwynk3.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-17", 
            "dogURL": "http://i.imgur.com/ni8vK0U.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-21", 
            "dogURL": "http://i.imgur.com/FPISssi.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-22", 
            "dogURL": "http://i.imgur.com/SzP5370.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-20", 
            "dogURL": "http://i.imgur.com/HoISzqN.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-25", 
            "dogURL": "http://i.imgur.com/rV4COi2.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-26", 
            "dogURL": "http://i.imgur.com/6WZFhJX.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-23", 
            "dogURL": "http://i.imgur.com/oHaP6I3.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-24", 
            "dogURL": "http://i.imgur.com/wlVWJpY.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-29", 
            "dogURL": "http://i.imgur.com/yDEa2a5.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-27", 
            "dogURL": "http://i.imgur.com/gFLzkPt.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-28", 
            "dogURL": "http://i.imgur.com/ULSe4AI.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-8", 
            "dogURL": "http://i.imgur.com/Ao9bR9A.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-7", 
            "dogURL": "http://i.imgur.com/C4rrJdn.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Retriever", 
            "dogName": "Retriever-9", 
            "dogURL": "http://i.imgur.com/R14AU0I.png", 
            "voteCount": 0
        }
    ], 
    "Yorkie": [
        {
            "breedName": "Yorkie", 
            "dogName": "Yorkie-3", 
            "dogURL": "http://i.imgur.com/qWLKy8a.jpg", 
            "voteCount": 2
        }, 
        {
            "breedName": "Yorkie", 
            "dogName": "Yorkie-1", 
            "dogURL": "http://i.imgur.com/oSieVUO.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Yorkie", 
            "dogName": "Yorkie-2", 
            "dogURL": "http://i.imgur.com/qtXIL.png", 
            "voteCount": 0
        }
    ]
}

* votes by breed

$ http GET localhost:8080/api/votes breedName==Yorkie -v
GET /api/votes?breedName=Yorkie HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: localhost:8080
User-Agent: HTTPie/0.9.9



HTTP/1.1 200 
Content-Type: application/json;charset=UTF-8
Date: Fri, 12 Jan 2018 03:43:47 GMT
Transfer-Encoding: chunked

{
    "Yorkie": [
        {
            "breedName": "Yorkie", 
            "dogName": "Yorkie-3", 
            "dogURL": "http://i.imgur.com/qWLKy8a.jpg", 
            "voteCount": 2
        }, 
        {
            "breedName": "Yorkie", 
            "dogName": "Yorkie-1", 
            "dogURL": "http://i.imgur.com/oSieVUO.png", 
            "voteCount": 0
        }, 
        {
            "breedName": "Yorkie", 
            "dogName": "Yorkie-2", 
            "dogURL": "http://i.imgur.com/qtXIL.png", 
            "voteCount": 0
        }
    ]
}
