package com.disney.studios;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.disney.studios","com.disney.studios.service"})
public class ApplicationTestConfig {
}
