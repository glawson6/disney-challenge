package com.disney.studios.service

import com.disney.studios.entity.Breed
import com.disney.studios.entity.Dog
import com.disney.studios.repository.DogRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.repository.query.Param
import spock.lang.Specification

class DogServiceImplTest extends Specification {
    private static final Logger logger = LoggerFactory.getLogger(DogServiceImplTest.class);

    def setup() {}          // run before every feature method
    def cleanup() {}        // run after every feature method
    def setupSpec() {}     // run before the first feature method
    def cleanupSpec() {}   // run after the last feature method
    final static String ID = '12345'

    def listOfDogs(){
        [ new Dog([id:ID,dogName: 'Fido', dogURL:'http://gogetit.name', breed: new Breed([id:'12345',breedName: 'Tuff'])]),
          new Dog([id:'78910',dogName: 'Lucky', dogURL:'http://gogetit.name', breed: new Breed([id:'12345',breedName: 'Smart'])])]
    }

    def createDogRepo(){
        def dogRepo = Stub(DogRepository){
            findDogsByBreed(_) >> listOfDogs().get(0)
            findAll() >> listOfDogs()
            findOne('12345') >> listOfDogs().get(0)
        }
        dogRepo
    }
    def createDogRepo2(){
        def dogRepo = Stub(DogRepository){
            findDogsByBreed(_) >> [listOfDogs().get(0)]
            findAll() >> listOfDogs()
            findOne('12345') >> listOfDogs().get(0)
        }
        dogRepo
    }

    def "FindDogsByBreed"() {
        given: "A DogServiceImpl "
        def dogService = new DogServiceImpl()
        def dogRepo = createDogRepo2()
        dogService.setDogRepository(dogRepo)
        def anyString = 'Tuff'

        when: "FindDogsByBreed called"
        def results = dogService.findDogsByBreed(anyString)

        then: "Correct number of dogs returned"
        results != null
        results instanceof List
        List<Dog> dogs = (List<Dog>)results
        dogs.size() == 1
        def dog = dogs.get(0)
        Breed breed = dog.breed
        breed.breedName == 'Tuff'
    }

    def "FindAll"() {
        given: "A DogServiceImpl "
        def dogService = new DogServiceImpl()
        def dogRepo = createDogRepo()
        dogService.setDogRepository(dogRepo)

        when: "findAll called"
        def results = dogService.findAll()

        then: "Correct number of dogs returned"
        results != null
        results instanceof List
        List<Dog> dogs = (List<Dog>)results
        dogs.size() == 2
    }

    def "FindbyId"() {
        given: "A DogServiceImpl "
        def dogService = new DogServiceImpl()
        def dogRepo = createDogRepo()
        dogService.setDogRepository(dogRepo)
        def anyString = 'Tuff'

        when: "findbyId called"
        def results = dogService.findbyId(ID)

        then: "Correct number of dogs returned"
        results != null
        results instanceof Dog
        Dog dog = (Dog)results
        Breed breed = dog.breed
        breed.breedName == 'Tuff'
    }
    def "FindbyId using null id"() {
        given: "A DogServiceImpl "
        def dogService = new DogServiceImpl()
        def dogRepo = createDogRepo()
        dogService.setDogRepository(dogRepo)
        def anyString = 'Tuff'

        when: "findbyId called"
        def results = dogService.findbyId(null)

        then: "Correct number of dogs returned"
        results == null
    }

    def "FindDogBreedMap all breeds"() {
        given: "A DogServiceImpl "
        def dogService = new DogServiceImpl()
        def dogRepo = createDogRepo()
        dogService.setDogRepository(dogRepo)

        when: "FindDogsByBreed called"
        def results = dogService.findDogBreedMap(null)

        then: "Correct number of dogs returned"
        results != null
        results instanceof Map
        Map<String, List<Dog>> dogMap = (Map<String, List<Dog>>)results
        dogMap.size() == 2
        dogMap.containsKey("Tuff")
        dogMap.containsKey("Smart")
    }

    def "FindDogBreedMap by breed"() {
        given: "A DogServiceImpl "
        def dogService = new DogServiceImpl()
        def dogRepo = createDogRepo2()
        dogService.setDogRepository(dogRepo)

        when: "FindDogsByBreed called"
        def results = dogService.findDogBreedMap('Tuff')

        then: "Correct number of dogs returned"
        results != null
        results instanceof Map
        Map<String, List<Dog>> dogMap = (Map<String, List<Dog>>)results
        dogMap.size() == 1
        dogMap.containsKey("Tuff")
        !dogMap.containsKey("Smart")
    }


}
