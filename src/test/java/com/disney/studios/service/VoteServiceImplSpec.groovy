package com.disney.studios.service

import com.disney.studios.Application
import com.disney.studios.ApplicationTestConfig
import com.disney.studios.dto.VoteDTO
import com.disney.studios.exception.DataNotFoundException
import com.disney.studios.repository.DogRepository
import com.disney.studios.repository.JudgeRepository
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner
import spock.lang.Specification

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [Application.class, ApplicationTestConfig.class])
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
class VoteServiceImplSpec extends Specification {
    private static final Logger logger = LoggerFactory.getLogger(VoteServiceImplSpec.class);

    def setup() {}          // run before every feature method
    def cleanup() {}        // run after every feature method
    def setupSpec() {}     // run before the first feature method
    def cleanupSpec() {}   // run after the last feature method

    @Autowired
    VoteService voteService;

    @Autowired
    DogRepository dogRepository

    @Autowired
    JudgeRepository judgeRepository

    @Test
    def "sanity"(){
        expect:
        voteService != null
    }

    @Test
    def "FindVoteByVoteCount"() {
        given:"Some votes"
        def voteDTO = new VoteDTO();
        def dogs = dogRepository.findAll()
        def judges = judgeRepository.findAll()
        voteDTO.setDogId(dogs.get(0).id)
        voteDTO.setJudgeId(judges.get(0).id)
        voteDTO.setScore(1)

        when:"voting"
        def result = voteService.vote(voteDTO)

        then:""
        result != null

    }

    @Test
    def "voting twice throws exception"() {
        given:"Some votes"
        def dog = dogRepository.findAll().get(0)
        def judge = judgeRepository.findAll().get(0)

        def voteDTO = new VoteDTO();
        voteDTO.setDogId(dog.id)
        voteDTO.setJudgeId(judge.id)
        voteDTO.setScore(1)

        def voteDTO2 = new VoteDTO();
        voteDTO2.setDogId(dog.id)
        voteDTO2.setJudgeId(judge.id)
        voteDTO2.setScore(1)

        when:"vote called twice"
        def result = voteService.vote(voteDTO)
        def result2 = voteService.vote(voteDTO2)

        then:""
        def e = thrown(Exception)
        e instanceof DataIntegrityViolationException

    }

    @Test
    def "voting without incorrect dog id throws exception"() {
        given:"A vote dto"
        def dog = dogRepository.findAll().get(0)
        def judge = judgeRepository.findAll().get(0)

        def voteDTO = new VoteDTO();
        voteDTO.setDogId('1122334')
        voteDTO.setJudgeId(judge.id)
        voteDTO.setScore(1)

        when:"vote called"
        def result = voteService.vote(voteDTO)

        then:""
        def e = thrown(Exception)
        e instanceof DataNotFoundException
        e.getMessage().contains("Dog")
        !e.getMessage().contains("Judge")

    }

    @Test
    def "voting without incorrect judge id throws exception"() {
        given:"A vote dto"
        def dog = dogRepository.findAll().get(0)
        def judge = judgeRepository.findAll().get(0)

        def voteDTO = new VoteDTO();
        voteDTO.setDogId(dog.id)
        voteDTO.setJudgeId('2532637')
        voteDTO.setScore(1)

        when:"vote called"
        def result = voteService.vote(voteDTO)

        then:""
        def e = thrown(Exception)
        e instanceof DataNotFoundException
        e.getMessage().contains("Judge")
        !e.getMessage().contains("Dog")

    }

    @Test
    def "findVoteByVoteCount"(){
        given: ""
        when: ""
        then: ""
    }
}

