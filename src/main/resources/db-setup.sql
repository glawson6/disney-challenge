

insert into breed values ('4f17f7b9-0356-4e2c-a87d-2e128de613c2','Labrador');
insert into breed values ('ba4bed5b-fc4e-4d8b-854c-597453b7871d','Pug');
insert into breed values ('ed57b814-2eaa-4ad0-aab2-78c43c3f296b','Retriever');
insert into breed values ('4c9803f8-44db-45b3-9abc-12cf39ff9795','Yorkie');

insert into judge (id, first_name, last_name) values ('e0012db9-5321-402c-a1a2-4eda635f2f11','Benjamin','MOORE');
insert into judge (id, first_name, last_name) values ('bc5946ff-a8bb-4f0b-b21a-93a795c9d635','Elijah','MARTIN');
insert into judge (id, first_name, last_name) values ('59e86130-a165-419e-8c76-7af114d4fd97','Daniel','JACKSON');
insert into judge (id, first_name, last_name) values ('b65ff52d-5971-4c30-81fa-48baa66c1c0e','Aiden','THOMPSON ');
insert into judge (id, first_name, last_name) values ('0973eb13-5a5b-404d-92fa-8987b7ea6899','Greg','LAWSON');
insert into judge (id, first_name, last_name) values ('00000000-0000-0000-0000-000000000000','SYSTEM','SYSTEM');
