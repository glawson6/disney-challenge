package com.disney.studios.controller;

import com.disney.studios.entity.Dog;
import com.disney.studios.service.DogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class DogController {
    private static final Logger logger = LoggerFactory.getLogger(DogController.class);

    @Autowired
    DogService dogService;

    public final static String INTERNAL_ERROR_MESSAGE = "System error. Please contact administrator.";

    @GetMapping("/dogs/{id}")
    ResponseEntity<?> getOneDog(@PathVariable (value = "id")String id){
        ResponseEntity<?> responseEntity = null;
        try{
            Dog dog = dogService.findbyId(id);
            if (null == dog){
                responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                responseEntity = new ResponseEntity<>(dog, HttpStatus.OK);
            }
        } catch (Exception e){
            logger.error(INTERNAL_ERROR_MESSAGE, e);
            responseEntity = new ResponseEntity<>(INTERNAL_ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @GetMapping("/dogs")
    ResponseEntity<?> getDogsByBreed(@RequestParam(value = "breedName", required = false)String breedName){
        ResponseEntity<?> responseEntity = null;
        try{
            List<Dog> dogs = null;
            if (null == breedName){
                dogs = dogService.findAll();
            } else {
                dogs = dogService.findDogsByBreed(breedName);
            }
            if (null == dogs || dogs.size() < 1){
                responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                responseEntity = new ResponseEntity<>(dogs, HttpStatus.OK);
            }
        } catch (Exception e){
            logger.error(INTERNAL_ERROR_MESSAGE, e);
            responseEntity = new ResponseEntity<>(INTERNAL_ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @GetMapping("/dogs/mapByBreed")
    ResponseEntity<?> mapDogsByBreed(@RequestParam(value = "breedName", required = false)String breedName){
        ResponseEntity<?> responseEntity = null;
        try{
            Map<String, List<Dog>> dogBreeds = dogService.findDogBreedMap(breedName);
            if (null == dogBreeds || dogBreeds.size() < 1){
                responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                responseEntity = new ResponseEntity<>(dogBreeds, HttpStatus.OK);
            }
        } catch (Exception e){
            logger.error(INTERNAL_ERROR_MESSAGE, e);
            responseEntity = new ResponseEntity<>(INTERNAL_ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
}
