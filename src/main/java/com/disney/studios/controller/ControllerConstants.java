package com.disney.studios.controller;

public class ControllerConstants {
    public final static String INTERNAL_ERROR_MESSAGE = "System error. Please contact administrator.";
    public final static String CONFLICT_ERROR_MESSAGE = "Conflict Please check that you are not trying to create an existing resource. ";
    public final static String VOTED = "Voted";
    public final static String DATA_NOT_AVAILABLE = "Data not available ";
}
