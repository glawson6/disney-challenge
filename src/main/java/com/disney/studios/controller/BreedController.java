package com.disney.studios.controller;


import com.disney.studios.entity.Breed;
import com.disney.studios.service.BreedService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@RestController
public class BreedController {

    private static final Logger logger = LoggerFactory.getLogger(BreedController.class);

    @Autowired
    BreedService breedService;

    //@Autowired
    //EntityLinks entityLinks;

    public final static String INTERNAL_ERROR_MESSAGE = "System error. Please contact administrator.";
    public final static String CONFLICT_ERROR_MESSAGE = "Conflict Please check that you are not trying to create an existing resource. ";

    @GetMapping("/breeds")
    ResponseEntity<?> getAllBreeds(@RequestParam(value = "page",required = false) @Min(0) final Integer page, @Max(100) @RequestParam(value = "size",required = false) final Integer size){
        PageRequest pageRequest = determinePageRequest(page,size);
        ResponseEntity<?> responseEntity = null;
        try{
            Page<Breed> pagedBreed = breedService.findAllBreeds(pageRequest);
            logger.info("Breeds size {}",pagedBreed.getTotalElements());
            if (null == pagedBreed || pagedBreed.getTotalElements() < 1){
                responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                responseEntity = new ResponseEntity<>(pagedBreed, HttpStatus.OK);
            }
        } catch (Exception e){
            logger.error(INTERNAL_ERROR_MESSAGE, e);
            responseEntity = new ResponseEntity<>(INTERNAL_ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    private PageRequest determinePageRequest(Integer page, Integer size) {
        PageRequest pageRequest = null;
        if (null != page && null != size){
            pageRequest = new PageRequest(page,size);
        }
        return pageRequest;
    }


    @PostMapping("/breeds")
    ResponseEntity<?> createBreed(@Valid @RequestBody Breed breed,Errors errors){
        if (errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        ResponseEntity<?> responseEntity = null;
        try{
            Breed savedBreed = breedService.save(breed);
            //HttpHeaders headers = new HttpHeaders();
            //Link link = entityLinks.linkToSingleResource(Breed.class, savedBreed.getId());
            //headers.setLocation(new URL(link.getHref()).toURI());
            responseEntity = new ResponseEntity<>(savedBreed, HttpStatus.CREATED);
        } catch (DataIntegrityViolationException e){
            StringBuilder sb = new StringBuilder(CONFLICT_ERROR_MESSAGE);
            sb.append(breed.toString());
            logger.error(sb.toString(), e);
            responseEntity = new ResponseEntity<>(sb.toString(), HttpStatus.CONFLICT);
        }catch (Exception e){
            logger.error(INTERNAL_ERROR_MESSAGE, e);
            responseEntity = new ResponseEntity<>(INTERNAL_ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    /*
    @GetMapping("/foos/{id}")
    @Validated
    public Foo findById(@PathVariable @Min(0) final long id) {
        return repo.findById(id).orElse(null);
    }
    */



}
