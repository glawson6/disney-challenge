package com.disney.studios.controller;

import com.disney.studios.dto.DogVoteDTO;
import com.disney.studios.dto.VoteDTO;
import com.disney.studios.entity.Breed;
import com.disney.studios.entity.Dog;
import com.disney.studios.exception.DataNotFoundException;
import com.disney.studios.service.VoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
public class VoteController {
    private static final Logger logger = LoggerFactory.getLogger(DogController.class);

    @Autowired
    VoteService voteService;


    @GetMapping("/votes")
    ResponseEntity<?> getVotesByAverage(@RequestParam(value = "breedName", required = false)String breedName){
        ResponseEntity<?> responseEntity = null;
        try{
            Map<String, List<DogVoteDTO>> voteNameMap = voteService.findVoteByVoteCount(breedName);
            //logger.info("voteNameMap size {}",voteNameMap.size());
            if (null == voteNameMap || voteNameMap.size() < 1){
                responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else {
                responseEntity = new ResponseEntity<>(voteNameMap, HttpStatus.OK);
            }
        } catch (Exception e){
            logger.error(ControllerConstants.INTERNAL_ERROR_MESSAGE, e);
            responseEntity = new ResponseEntity<>(ControllerConstants.INTERNAL_ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @PostMapping("/votes")
    ResponseEntity<?> createVotesByAverage(@Valid @RequestBody VoteDTO voteDTO, Errors errors){
        if (errors.hasErrors()) {
            return new ResponseEntity(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        ResponseEntity<?> responseEntity = null;
        try{
            VoteDTO vote = voteService.vote(voteDTO);
            responseEntity = new ResponseEntity<>(ControllerConstants.VOTED, HttpStatus.CREATED);
        } catch (DataNotFoundException e) {
            StringBuilder sb = new StringBuilder(ControllerConstants.DATA_NOT_AVAILABLE);
            sb.append(voteDTO.toString());
            logger.error(sb.toString(), e);
            responseEntity = new ResponseEntity<>(sb.toString(), HttpStatus.BAD_REQUEST);
        }catch (DataIntegrityViolationException e){
            StringBuilder sb = new StringBuilder(ControllerConstants.CONFLICT_ERROR_MESSAGE);
            sb.append(voteDTO.toString());
            logger.error(sb.toString(), e);
            responseEntity = new ResponseEntity<>(sb.toString(), HttpStatus.CONFLICT);
        }catch (Exception e){
            logger.error(ControllerConstants.INTERNAL_ERROR_MESSAGE, e);
            responseEntity = new ResponseEntity<>(ControllerConstants.INTERNAL_ERROR_MESSAGE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

}
