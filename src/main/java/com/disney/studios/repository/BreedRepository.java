package com.disney.studios.repository;

import com.disney.studios.entity.Breed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository("breedRepository")
@Transactional
public interface BreedRepository extends JpaRepository<Breed, String>, JpaSpecificationExecutor<Breed> {

    Page<Breed> findAll(Pageable pageable);

    @Query("select b from Breed as b where b.breedName = :breedName ")
    Breed findByBreedName(@Param("breedName") String breedName);
}
