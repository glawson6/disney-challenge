package com.disney.studios.repository;

import com.disney.studios.entity.Judge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository("judgeRepository")
public interface JudgeRepository extends JpaRepository<Judge, String> {
}
