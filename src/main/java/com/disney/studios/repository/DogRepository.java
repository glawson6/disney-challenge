package com.disney.studios.repository;

import com.disney.studios.entity.Dog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("dogRepository")
public interface DogRepository extends JpaRepository<Dog, String> {

    @Query("select d from Dog as d where d.breed.breedName = :breedName order by d.dogName ")
    List<Dog> findDogsByBreed(@Param("breedName") String breedName);

    List<Dog> findAll();
}
