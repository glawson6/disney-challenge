package com.disney.studios.repository;

import com.disney.studios.entity.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository("voteRepository")
@Transactional
public interface VoteRepository extends JpaRepository<Vote, String> {

    @Query("SELECT v.dog.dogName AS name, AVG(v.score) AS average FROM Vote AS v GROUP BY v.dog.dogName ORDER BY average DESC")
    List<Object[]> findByAverage();

    @Query("SELECT DISTINCT v, SUM(v.score) AS dogSum, v.dog.breed FROM Vote AS v GROUP BY  v.dog.breed,v ORDER BY dogSum DESC")
    List<Object[]> findByVoteCount();

    @Query("SELECT DISTINCT v, SUM(v.score) AS dogSum, v.dog.breed FROM Vote AS v where v.dog.breed.breedName = :breedName GROUP BY v.dog.breed,v ORDER BY dogSum DESC")
    List<Object[]> findByVoteCountAndName(@Param("breedName") String breedName);


    // TODO This is a hack. Needs to be fixed. This should return a list.
    List<Vote> findByDogId(@Param("dogId") String dogId);


    Vote save(Vote vote);
}
