package com.disney.studios.service;

import com.disney.studios.entity.Dog;
import com.disney.studios.repository.DogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component("dogService")
public class DogServiceImpl implements DogService {
    private static final Logger logger = LoggerFactory.getLogger(DogServiceImpl.class);

    @Autowired
    DogRepository dogRepository;

    @Override
    public List<Dog> findDogsByBreed(String breedName) {
        return dogRepository.findDogsByBreed(breedName);
    }

    @Override
    public List<Dog> findAll() {
        return dogRepository.findAll();
    }

    @Override
    public Dog findbyId(String id) {
        Dog dog = null;
        if (id != null){
            dog = dogRepository.findOne(id);
        }
        return dog;
    }

    @Override
    public Map<String, List<Dog>> findDogBreedMap(String breedName) {

        Map<String, List<Dog>> dogsByBreed = null;
        if (null == breedName){
            List<Dog> allDogs = this.findAll();
            dogsByBreed = allDogs
                    .stream()
                    .collect(Collectors.groupingBy(dog -> dog.getBreed().getBreedName()));
        } else {
            List<Dog> dogs = this.findDogsByBreed(breedName);
            if (null != dogs && !dogs.isEmpty()) {
                dogsByBreed = new HashMap<>();
                dogsByBreed.put(breedName, dogs);
            }
        }

        return dogsByBreed;
    }

    public DogRepository getDogRepository() {
        return dogRepository;
    }

    public void setDogRepository(DogRepository dogRepository) {
        this.dogRepository = dogRepository;
    }
}
