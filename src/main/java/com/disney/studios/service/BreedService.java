package com.disney.studios.service;

import com.disney.studios.entity.Breed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface BreedService {
    Page<Breed> findAllBreeds(PageRequest pageRequest);
    Breed save(Breed breed);
}
