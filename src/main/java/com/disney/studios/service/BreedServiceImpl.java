package com.disney.studios.service;

import com.disney.studios.entity.Breed;
import com.disney.studios.repository.BreedRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("breedService")
public class BreedServiceImpl implements BreedService {

    private static final Logger logger = LoggerFactory.getLogger(BreedServiceImpl.class);

    @Autowired
    public BreedServiceImpl(BreedRepository breedRepository) {
        logger.info("Creating breedService");
        this.breedRepository = breedRepository;
    }

    BreedRepository breedRepository;

    @Override
    public Page<Breed> findAllBreeds(PageRequest pageRequest) {
        logger.info("In finaAllBreeds");
        if (null == pageRequest){
            return breedRepository.findAll(new PageRequest(0,20));
        } else {
            return breedRepository.findAll(pageRequest);
        }
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public Breed save(Breed breed) {
        return breedRepository.saveAndFlush(breed);
    }
}
