package com.disney.studios.service;


import com.disney.studios.dto.DogVoteDTO;
import com.disney.studios.dto.VoteDTO;
import com.disney.studios.entity.Dog;
import com.disney.studios.entity.Vote;

import java.util.List;
import java.util.Map;

public interface VoteService {
    Map<String, List<DogVoteDTO>> findVoteByVoteCount(String breedName);
    VoteDTO vote(VoteDTO voteDTO);

}
