package com.disney.studios.service;

import com.disney.studios.dto.DogVoteDTO;
import com.disney.studios.dto.VoteDTO;
import com.disney.studios.entity.Dog;
import com.disney.studios.entity.Judge;
import com.disney.studios.entity.Vote;
import com.disney.studios.exception.DataNotFoundException;
import com.disney.studios.repository.DogRepository;
import com.disney.studios.repository.JudgeRepository;
import com.disney.studios.repository.VoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Component("voteService")
@Transactional
public class VoteServiceImpl implements VoteService {
    private static final Logger logger = LoggerFactory.getLogger(VoteServiceImpl.class);

    @Autowired
    VoteRepository voteRepository;


    @Autowired
    DogRepository dogRepository;

    @Autowired
    JudgeRepository judgeRepository;


    // TODO. I know it is not working because the distinct is using the id of the vote class. I need more time to refactor this to
    // TODO a proper query or structure to handle the duplicates. I could also massage The DogVoteDTO List.
    @Override
    public Map<String, List<DogVoteDTO>> findVoteByVoteCount(String breedName) {
        logger.debug("Got breedName {}",breedName);
        List<Object[]> results = null;
        if (null != breedName){

            results = voteRepository.findByVoteCountAndName(breedName);
        } else {
            results = voteRepository.findByVoteCount();
        }
        logger.debug("results {}",results.toString());

        // TODO NEED MORE TIME TO IMPLEMENT RIGHT COLLECT HERE


        Map<String, List<DogVoteDTO>> votes = results.stream()
                .map(res -> convertToVote(res))
                .collect(Collectors.groupingBy(DogVoteDTO::getBreedName));


        Map<String, List<DogVoteDTO>> dogVotes = votes.entrySet().stream()
                .map(entry -> squashList(entry))
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));

        return dogVotes;
    }

    private Map.Entry<String, List<DogVoteDTO>> squashList(Map.Entry<String, List<DogVoteDTO>> entry) {

        List<DogVoteDTO> dogVoteDTOS = entry.getValue();
        Map<String,DogVoteDTO> breedMap = new HashMap<>();
        for (DogVoteDTO dogVoteDTO: dogVoteDTOS){
            DogVoteDTO foundDog = breedMap.get(dogVoteDTO.getDogName());
            if (foundDog != null){
                foundDog.setVoteCount(foundDog.getVoteCount()+dogVoteDTO.getVoteCount());
            } else {
                foundDog = dogVoteDTO;
            }
            breedMap.put(foundDog.getDogName(), foundDog);
        }
        List<DogVoteDTO> list = new ArrayList<DogVoteDTO>(breedMap.values());
        Collections.sort(list, new DogVoteDTO());
        entry.setValue(list);
        return entry;
    }


    private DogVoteDTO convertToVote(Object[] res) {
        DogVoteDTO voteDTO = new DogVoteDTO();

        Vote vote = (Vote)res[0];
        Long count = (Long)res[1];
        //Dog dog = (Dog)res[2];
        Dog dog = vote.getDog();
        voteDTO.setBreedName(dog.getBreed().getBreedName());
        voteDTO.setDogName(dog.getDogName());
        voteDTO.setDogURL(dog.getDogURL());
        voteDTO.setVoteCount(count==null?0:count.intValue());
        return voteDTO;
    }



    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public VoteDTO vote(VoteDTO voteDTO) {

        if (null != voteDTO){
            Vote vote = new Vote();
            Dog dog = dogRepository.findOne(voteDTO.getDogId());
            if (dog == null){
                throw new DataNotFoundException("Dog not found");
            }
            Judge judge = judgeRepository.findOne(voteDTO.getJudgeId());
            if (null == judge){
                throw new DataNotFoundException("Judge not found");
            }
            vote.setDog(dog);
            vote.setJudge(judge);
            vote.setScore(voteDTO.getScore());
            vote = voteRepository.saveAndFlush(vote);
            logger.debug("We have a vote {}",vote.toString());

            logger.debug("We have a vote {}",vote.toString());
        }
        return voteDTO;
    }

    public VoteRepository getVoteRepository() {
        return voteRepository;
    }

    public void setVoteRepository(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    public DogRepository getDogRepository() {
        return dogRepository;
    }

    public void setDogRepository(DogRepository dogRepository) {
        this.dogRepository = dogRepository;
    }

    public JudgeRepository getJudgeRepository() {
        return judgeRepository;
    }

    public void setJudgeRepository(JudgeRepository judgeRepository) {
        this.judgeRepository = judgeRepository;
    }
}
