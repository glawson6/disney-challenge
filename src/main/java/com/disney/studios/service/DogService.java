package com.disney.studios.service;


import com.disney.studios.entity.Dog;

import java.util.List;
import java.util.Map;

public interface DogService {
    List<Dog> findDogsByBreed(String breedName);
    List<Dog> findAll();

    Dog findbyId(String id);
    Map<String, List<Dog>> findDogBreedMap(String breedName);
}
