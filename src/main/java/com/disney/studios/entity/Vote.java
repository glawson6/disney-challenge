package com.disney.studios.entity;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "vote",uniqueConstraints={@UniqueConstraint(columnNames = {"JUDGE_ID","DOG_ID"})})
public class Vote extends AbstractEntity{

    private static final long serialVersionUID = 8174158671891121486L;
    @Min(-1)
    @Max(1)
    @Column(name="score", nullable=false)
    private Integer score;

    @Basic(optional = false)
    @JoinColumn(name = "DOG_ID", referencedColumnName = "ID")
    @ManyToOne
    private Dog dog;

    @Basic(optional = false)
    @JoinColumn(name = "JUDGE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Judge judge;


    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    public Judge getJudge() {
        return judge;
    }

    public void setJudge(Judge judge) {
        this.judge = judge;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Vote{");
        sb.append("score=").append(score);
        sb.append(", dog=").append(dog);
        sb.append(", judge=").append(judge);
        sb.append('}');
        return sb.toString();
    }
}
