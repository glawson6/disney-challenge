package com.disney.studios.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "breed",uniqueConstraints={@UniqueConstraint(columnNames = {"BREED_NAME"})})
public class Breed extends AbstractEntity {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "BREED_NAME")
    private String breedName;


    public String getBreedName() {
        return breedName;
    }

    public void setBreedName(String breedName) {
        this.breedName = breedName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Breed breed = (Breed) o;
        return Objects.equals(breedName, breed.breedName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), breedName);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Breed{");
        sb.append("id='").append(getId()).append('\'');
        sb.append("breedName='").append(breedName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
