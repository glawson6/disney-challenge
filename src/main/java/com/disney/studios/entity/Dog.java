package com.disney.studios.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Table(name = "dog",uniqueConstraints={@UniqueConstraint(columnNames = {"DOG_URL","BREED_ID"})})
public class Dog extends AbstractEntity {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "DOG_NAME")
    private String dogName;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "DOG_URL")
    private String dogURL;

    @Basic(optional = false)
    @JoinColumn(name = "BREED_ID", referencedColumnName = "ID")
    @ManyToOne
    private Breed breed;


    public String getDogName() {
        return dogName;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    public String getDogURL() {
        return dogURL;
    }

    public void setDogURL(String dogURL) {
        this.dogURL = dogURL;
    }



}
