package com.disney.studios.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.proxy.HibernateProxy;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.PojomaticPolicy;
import org.pojomatic.annotations.Property;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public class AbstractEntity implements Serializable {
    private static final long serialVersionUID = 2535090450811888936L;
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "ID")
    @Property(policy = PojomaticPolicy.TO_STRING)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String toString() {
        return Pojomatic.toString(this.getEntity());
    }

    public boolean equals(Object obj) {
        return Pojomatic.equals(this.getEntity(), obj);
    }

    public int hashCode() {
        return Pojomatic.hashCode(this.getEntity());
    }

    // Hack to get correct class for toString, equals, and hashcode
    private Object getEntity() {
        return this instanceof HibernateProxy ? ((HibernateProxy) HibernateProxy.class.cast(this)).getHibernateLazyInitializer().getImplementation() : this;
    }
}
