package com.disney.studios.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class VoteDTO {

    @NotNull
    private String judgeId;

    @NotNull
    private String dogId;

    @Min(-1)
    @Max(1)
    @NotNull
    private Integer score;

    public String getJudgeId() {
        return judgeId;
    }

    public String getDogId() {
        return dogId;
    }

    public Integer getScore() {
        return score;
    }

    public void setJudgeId(String judgeId) {
        this.judgeId = judgeId;
    }

    public void setDogId(String dogId) {
        this.dogId = dogId;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("VoteDTO{");
        sb.append("judgeId='").append(judgeId).append('\'');
        sb.append(", dogId='").append(dogId).append('\'');
        sb.append(", score=").append(score);
        sb.append('}');
        return sb.toString();
    }
}
