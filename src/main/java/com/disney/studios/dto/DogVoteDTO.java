package com.disney.studios.dto;

import java.util.Comparator;
import java.util.Objects;

public class DogVoteDTO implements Comparator<DogVoteDTO> {

    private String dogName;
    private String dogURL;
    private String breedName;
    private Integer voteCount;

    public String getDogName() {
        return dogName;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public String getDogURL() {
        return dogURL;
    }

    public void setDogURL(String dogURL) {
        this.dogURL = dogURL;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public String getBreedName() {
        return breedName;
    }

    public void setBreedName(String breedName) {
        this.breedName = breedName;
    }

    @Override
    public int compare(DogVoteDTO o1, DogVoteDTO o2) {
        return o2.getVoteCount().compareTo(o1.getVoteCount());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DogVoteDTO voteDTO = (DogVoteDTO) o;
        return Objects.equals(dogName, voteDTO.dogName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dogName);
    }
}
