package com.disney.studios;

import com.disney.studios.entity.Breed;
import com.disney.studios.entity.Dog;
import com.disney.studios.entity.Judge;
import com.disney.studios.entity.Vote;
import com.disney.studios.repository.*;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Loads stored objects from the file system and builds up
 * the appropriate objects to add to the data source.
 *
 * Created by fredjean on 9/21/15.
 */
@Component("petLoader")
public class PetLoader{
    // Resources to the different files we need to load.
    ResourceLoader resourceLoader = new DefaultResourceLoader();

    @Value("classpath:data/labrador.txt")
    private Resource labradors;

    @Value("classpath:data/pug.txt")
    private Resource pugs;

    @Value("classpath:data/retriever.txt")
    private Resource retrievers;

    @Value("classpath:data/yorkie.txt")
    private Resource yorkies;

    @Autowired
    BreedRepository breedRepository;

    @Autowired
    DogRepository dogRepository;


    @Autowired
    JudgeRepository judgeRepository;

    @Autowired
    VoteRepository voteRepository;

    @Autowired
    DataSource dataSource;

    @PostConstruct
    public void load() throws Exception {
        voteRepository.deleteAll();
        dogRepository.deleteAll();
        breedRepository.deleteAll();
        judgeRepository.deleteAll();
        Resource setupResource =  resourceLoader.getResource("classpath:db-setup.sql");
        //String setupResourceStr = setupResource.inputStream.text
        ScriptUtils.executeSqlScript(dataSource.getConnection(), setupResource);

        loadBreed(breedRepository.findByBreedName("Labrador"), labradors);
        loadBreed(breedRepository.findByBreedName("Pug"), pugs);
        loadBreed(breedRepository.findByBreedName("Retriever"), retrievers);
        loadBreed(breedRepository.findByBreedName("Yorkie"), yorkies);
    }



    /**
     * Load the different breeds into the data source after
     * the application is ready.
     *
     * @throws Exception In case something goes wrong while we load the breeds.
     */
    //@Override
    /*
    public void afterPropertiesSet() throws Exception {
        loadBreed("Labrador", labradors);
        loadBreed("Pug", pugs);
        loadBreed("Retriever", retrievers);
        loadBreed("Yorkie", yorkies);
    }
    */

    private static final String SYSTEM_ID = "00000000-0000-0000-0000-000000000000";
    private static final Integer ZERO_SCORE = new Integer(0);

    /**
     * Reads the list of dogs in a category and (eventually) add
     * them to the data source.
     * @param breed The breed that we are loading.
     * @param source The file holding the breeds.
     * @throws IOException In case things go horribly, horribly wrong.
     */
    private void loadBreed(Breed breed, Resource source) throws IOException {
        int cnt = 1;
        Judge systemJudge = judgeRepository.findOne(SYSTEM_ID);
        try ( BufferedReader br = new BufferedReader(new InputStreamReader(source.getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
                /* TODO: Create appropriate objects and save them to
                 *       the datasource.
                 */
                StringBuilder dogName = new StringBuilder(breed.getBreedName());
                dogName.append("-").append(cnt++);
                Dog dog = new Dog();
                dog.setBreed(breed);
                dog.setDogURL(line);
                dog.setDogName(dogName.toString());
                dog = dogRepository.save(dog);
                Vote vote = new Vote();
                vote.setJudge(systemJudge);
                vote.setDog(dog);
                vote.setScore(ZERO_SCORE);
                voteRepository.save(vote);


            }
        }
    }

    public Resource getLabradors() {
        return labradors;
    }

    public void setLabradors(Resource labradors) {
        this.labradors = labradors;
    }

    public Resource getPugs() {
        return pugs;
    }

    public void setPugs(Resource pugs) {
        this.pugs = pugs;
    }

    public Resource getRetrievers() {
        return retrievers;
    }

    public void setRetrievers(Resource retrievers) {
        this.retrievers = retrievers;
    }

    public Resource getYorkies() {
        return yorkies;
    }

    public void setYorkies(Resource yorkies) {
        this.yorkies = yorkies;
    }

    public BreedRepository getBreedRepository() {
        return breedRepository;
    }

    public void setBreedRepository(BreedRepository breedRepository) {
        this.breedRepository = breedRepository;
    }

    public DogRepository getDogRepository() {
        return dogRepository;
    }

    public void setDogRepository(DogRepository dogRepository) {
        this.dogRepository = dogRepository;
    }

    public JudgeRepository getJudgeRepository() {
        return judgeRepository;
    }

    public void setJudgeRepository(JudgeRepository judgeRepository) {
        this.judgeRepository = judgeRepository;
    }

    public VoteRepository getVoteRepository() {
        return voteRepository;
    }

    public void setVoteRepository(VoteRepository voteRepository) {
        this.voteRepository = voteRepository;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
