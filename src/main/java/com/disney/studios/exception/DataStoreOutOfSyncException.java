package com.disney.studios.exception;

public class DataStoreOutOfSyncException extends RuntimeException {
    public DataStoreOutOfSyncException() {
    }

    public DataStoreOutOfSyncException(String message) {
        super(message);
    }

    public DataStoreOutOfSyncException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataStoreOutOfSyncException(Throwable cause) {
        super(cause);
    }

    public DataStoreOutOfSyncException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
